#ifndef CONTENT_H_
#define CONTENT_H_

#include <climits>
#include <string>

#include "utilities/Container.hpp"

namespace librarysystem {

    namespace model {

        namespace book {

            class Content {
                std::shared_ptr<utilities::Container<std::string>> rows;

            public:
                Content(const std::shared_ptr<utilities::Container<std::string>> &rows = std::shared_ptr<utilities::Container<std::string>>());

                Content(const Content &other) = delete;

                int getNumberOfRows() const;

                std::string getRow(int idx) const;

                static const int MAX_ROWS = INT_MAX;

                void print() const;
            };

        };

    };

};

#endif
