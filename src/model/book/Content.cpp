#include "Content.h"

#include <iostream>

librarysystem::model::book::Content::Content(const std::shared_ptr<librarysystem::utilities::Container<std::string>> &rows) {
    this->rows = rows;
}

int librarysystem::model::book::Content::getNumberOfRows() const {
    return rows->getSize();
}

std::string librarysystem::model::book::Content::getRow(int idx) const {
    return this->rows->getElement(idx);
}

void librarysystem::model::book::Content::print() const {
    for (int i = 0; i < rows->getSize(); i++) {
        std::cout << rows->getElement(i) << std::endl;
    }
}
