#include "Book.h"

#include <cmath>
#include <utility>

#include "model/person/Author.h"
#include "model/person/Reader.h"

using namespace librarysystem::model::person;


librarysystem::model::book::Book::Book() : content(), price(PRICE_DEFAULT), publicationYear(YEAR_DEFAULT) {
}

librarysystem::model::book::Book::Book(std::string title, const std::shared_ptr<Content> &content,
                                       std::string author, const double &price,
                                       const std::shared_ptr<utilities::Container<double>> &ratings,
                                       int publicationYear) : title(std::move(title)),
                                                              author(std::move(author)),
                                                              price(price),
                                                              publicationYear(publicationYear) {
    this->content = content;
    this->ratings = ratings;
}

librarysystem::model::book::Book::Book(std::string title, int publicationYear,
                                       std::string author, int price) : title(std::move(title)),
                                                                        author(std::move(author)),
                                                                        price(price),
                                                                        publicationYear(publicationYear) {
}

librarysystem::model::book::Book::Book(std::string title, std::string author) : title(std::move(title)),
                                                                                author(std::move(author)),
                                                                                price(PRICE_DEFAULT),
                                                                                publicationYear(YEAR_DEFAULT) {
}

bool librarysystem::model::book::Book::operator==(const Book &other) const {
    return title == other.title && author == other.author;
}

const std::string &librarysystem::model::book::Book::getAuthor() const {
    return author;
}

const std::shared_ptr<librarysystem::model::book::Content> &librarysystem::model::book::Book::getContent() const {
    return content;
}

double librarysystem::model::book::Book::getPrice() const {
    return price;
}

int librarysystem::model::book::Book::getPublicationYear() const {
    return publicationYear;
}

const std::shared_ptr<librarysystem::utilities::Container<double>> &
librarysystem::model::book::Book::getRatings() const {
    return ratings;
}

const std::string &librarysystem::model::book::Book::getTitle() const {
    return title;
}

int librarysystem::model::book::Book::getNumberOfRows() const {
    return content->getNumberOfRows();
}

int librarysystem::model::book::Book::getNumberOfTimesItWasReadByReaders(
        const std::shared_ptr<utilities::Container<std::shared_ptr<Reader>>> &readers) const {
    int count = 0;
    for (int i = 0; i < readers->getSize(); i++) {
        std::shared_ptr<Reader> singleReader = readers->getElement(i);
        count += getNumberOfTimesItWasReadBySingleReader(singleReader);
    }

    return count;
}

int librarysystem::model::book::Book::getNumberOfTimesItWasReadByAuthors(
        const std::shared_ptr<utilities::Container<std::shared_ptr<Author>>> &authors) const {
    int count = 0;
    for (int i = 0; i < authors->getSize(); i++) {
        std::shared_ptr<Reader> singleAuthor = authors->getElement(i);
        count += getNumberOfTimesItWasReadBySingleReader(singleAuthor);
    }

    return count;
}

int
librarysystem::model::book::Book::getNumberOfTimesItWasReadBySingleReader(const std::shared_ptr<Reader> &reader) const {
    int count = 0;
    const std::shared_ptr<utilities::Container<std::shared_ptr<Book>>> &readBooks = reader->getReadBooks();
    for (int j = 0; j < readBooks->getSize(); j++) {
        std::shared_ptr<Book> book = readBooks->getElement(j);
        if (this == book.get()) {
            count++;
        }
    }

    return count;
}

double librarysystem::model::book::Book::getOverallRating() const {
    double ratingSum = 0.0;
    for (int i = 0; i < ratings->getSize(); i++) {
        ratingSum += ratings->getElement(i);
    }

    double rating = ratingSum / ratings->getSize();
    double floorRating = floor(rating);
    if (rating - floorRating < 0.5) {
        return floorRating;
    } else {
        return floorRating + 0.5;
    }

    return rating;
}

void librarysystem::model::book::Book::addRating(double rating) {
    ratings->insert(rating);
}

void librarysystem::model::book::Book::setPrice(double price) {
    this->price = price;
}

void librarysystem::model::book::Book::setRatings(const std::shared_ptr<utilities::Container<double>> &ratings) {
    this->ratings = ratings;
}
