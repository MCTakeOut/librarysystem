#ifndef BOOK_H_
#define BOOK_H_

#include <memory>
#include <string>

#include "utilities/Container.hpp"
#include "Content.h"

namespace librarysystem {

    namespace model {
        namespace person {
            class Author;

            class Reader;
        }
    }
}

namespace librarysystem {

    namespace model {

        namespace book {

            class Book {
                static constexpr double PRICE_DEFAULT = 0.0;
                static const int YEAR_DEFAULT = 0;

                std::string title;
                std::shared_ptr<model::book::Content> content;
                std::string author;
                double price;
                std::shared_ptr<utilities::Container<double>> ratings;
                int publicationYear;


            public:
                Book();

                Book(std::string title, const std::shared_ptr<Content> &content,
                     std::string author, const double &price = 0.0,
                     const std::shared_ptr<utilities::Container<double>> &ratings = std::make_shared<utilities::Container<double>>(),
                     int publicationYear = 0);

                Book(std::string title, int publicationYear,
                     std::string author, int price);

                Book(std::string title, std::string author);

                Book(const Book &other) = delete;

                bool operator==(const Book &other) const;

                const std::string &getTitle() const;

                const std::shared_ptr<Content> &getContent() const;

                const std::string &getAuthor() const;

                double getPrice() const;

                const std::shared_ptr<utilities::Container<double>> &getRatings() const;

                int getPublicationYear() const;

                int getNumberOfRows() const;

                int getNumberOfTimesItWasReadByReaders(
                        const std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>> &readers) const;

                int getNumberOfTimesItWasReadByAuthors(
                        const std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>> &authors) const;

                double getOverallRating() const;

                void addRating(double rating);

                void setPrice(double price);

                void setRatings(const std::shared_ptr<utilities::Container<double>> &ratings);

            private:
                int getNumberOfTimesItWasReadBySingleReader(const std::shared_ptr<model::person::Reader> &reader) const;
            };

        };

    };

};

#endif
