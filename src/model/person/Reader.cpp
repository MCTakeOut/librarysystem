#include "Reader.h"
#include "model/book/Book.h"

using namespace librarysystem::model::person;

Reader::Reader(const std::string &name, int age, const std::string &nationality,
               const std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>> &readBooks)
        : Person(name, nationality, age) {
    this->readBooks = readBooks;
}

std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>> &
Reader::getReadBooks() {
    return readBooks;
}

const std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>> &
Reader::getReadBooks() const {
    return readBooks;
}

bool Reader::hasReadBook(const std::string &title, const std::string &author) const {
    std::shared_ptr<librarysystem::model::book::Book> fictiveBook = std::make_shared<librarysystem::model::book::Book>(
            title, author);
    int bookIdx = readBooks->search(fictiveBook);
    return bookIdx != readBooks->INVALID_INDEX;
}

void Reader::readBook(const std::shared_ptr<librarysystem::model::book::Book> &book) {
    readBooks->insert(book);
}
