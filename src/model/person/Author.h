#ifndef AUTHOR_H_
#define AUTHOR_H_

#include <memory>
#include <string>

#include "utilities/Container.hpp"
#include "Reader.h"

namespace librarysystem {

    namespace model {

        namespace person {

            class Author : public Reader {
                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> writtenBooks;

            public:
                Author(const std::string &name = "", const int &age = 0, const std::string &nationality = "",
                       const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &
                       writtenBooks = std::make_shared<utilities::Container<
                               std::shared_ptr<model::book::Book>>>(),
                       const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &readBooks = std::make_shared<utilities::Container<
                               std::shared_ptr<model::book::Book>>>()
                );

                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &getWrittenBooks();

                const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &
                getWrittenBooks() const;

                void setWrittenBooks(
                        std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &writtenBooks);

                bool hasWrittenBook(const std::string &title, const std::string &author) const;

                void writeBook(std::shared_ptr<model::book::Book> book);
            };

        };

    };

};


#endif
