#include "Author.h"
#include "model/book/Book.h"
#include <string.h>

using namespace librarysystem::model::book;
using librarysystem::utilities::Container;

librarysystem::model::person::Author::Author(const std::string &name, const int &age, const std::string &nationality,
                                             const std::shared_ptr<Container<std::shared_ptr<Book>>> &writtenBooks,
                                             const std::shared_ptr<Container<std::shared_ptr<Book>>> &readBooks)
        : Reader(name, age, nationality, readBooks) {
    this->writtenBooks = writtenBooks;
}

std::shared_ptr<Container<std::shared_ptr<Book>>> &librarysystem::model::person::Author::getWrittenBooks() {
    return writtenBooks;
}

const std::shared_ptr<Container<std::shared_ptr<Book>>> &librarysystem::model::person::Author::getWrittenBooks() const {
    return writtenBooks;
}

bool librarysystem::model::person::Author::hasWrittenBook(const std::string &title, const std::string &author) const {
    std::shared_ptr<Book> book = std::make_shared<Book>(title, author);
    int bookIdx = writtenBooks->search(book);
    return bookIdx != writtenBooks->INVALID_INDEX;
}

void
librarysystem::model::person::Author::setWrittenBooks(std::shared_ptr<Container<std::shared_ptr<Book>>> &writtenBooks) {
    this->writtenBooks = writtenBooks;
}

void librarysystem::model::person::Author::writeBook(std::shared_ptr<Book> book) {
    writtenBooks->insert(book);
}
// author::~author() {
// 	// TODO Auto-generated destructor stub
// }
