#ifndef READER_H_
#define READER_H_

#include <memory>
#include <string>
#include <model/book/Book.h>
#include "utilities/Container.hpp"
#include "model/person/Person.h"


namespace librarysystem {

    namespace model {

        namespace person {

            class Reader : public Person {
                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> readBooks;

            public:
                Reader(const std::string &name = "", int age = 0, const std::string &nationality = "",
                       const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &readBooks =
                       std::make_shared<utilities::Container<std::shared_ptr<model::book::Book>>>());

                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &getReadBooks();

                const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &getReadBooks() const;

                bool hasReadBook(const std::string &title, const std::string &author) const;

                void readBook(const std::shared_ptr<model::book::Book>& book);
            };

        };

    };

};

#endif
