#include "LibraryLoader.h"

#include <cassert>
#include <fstream>
#include <iostream>

#include "model/person/Author.h"
#include "model/book/Book.h"
#include "controller/library/Library.h"
#include "utilities/StringToIntegerConverter.h"
#include "utilities/StringToDoubleConverter.h"

using namespace librarysystem::controller::library;
using namespace librarysystem::model::book;
using namespace librarysystem::model::person;

librarysystem::controller::deserializing::LibraryLoader::LibraryLoader() {
}

std::shared_ptr<Library>
librarysystem::controller::deserializing::LibraryLoader::load(const std::string &absoluteFilename) {
    std::shared_ptr<utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>> books;
    std::shared_ptr<utilities::Container<std::shared_ptr<librarysystem::model::person::Author>>> authors;
    std::shared_ptr<utilities::Container<std::shared_ptr<librarysystem::model::person::Reader>>> readers;
    std::ifstream fin(absoluteFilename);
    std::string line;
    while (getline(fin, line, '\n')) {
        if (line == "####### BOOKS #######") {
            books = readBooks(fin);
        } else if (line == "####### AUTHORS #######") {
            authors = readAuthors(fin, books);
        } else if (line == "####### READERS #######") {
            readers = readReaders(fin, books);
        } else {
            std::cerr << "Section name: "
                      << "|" << line << "|" << std::endl;
            assert(false && "Invalid file format - invalid section name");
        }
    }
    return std::make_shared<Library>(books, authors, readers);
}

std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>>
librarysystem::controller::deserializing::LibraryLoader::readBooks(std::ifstream &fin) {
    std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> books =
            std::make_shared<utilities::Container<std::shared_ptr<model::book::Book>>>();

    std::string line;
    getline(fin, line, '\n');
    while (line != "####### END BOOKS #######") {
        if (line == "##### BOOK #####") {
            std::shared_ptr<Book> singleBook = readSingleBook(fin);
            books->insert(singleBook);
        } else {
            assert(false && "Invalid file format");
        }
        getline(fin, line, '\n');
    }
    return books;
}


std::shared_ptr<Book> librarysystem::controller::deserializing::LibraryLoader::readSingleBook(std::ifstream &fin) {
    std::shared_ptr<std::string> title;
    std::shared_ptr<int> publicationYear;
    std::shared_ptr<double> price;
    std::shared_ptr<utilities::Container<double>> ratings;
    std::shared_ptr<model::book::Content> content;
    std::shared_ptr<std::string> authorName;
    std::string line;
    getline(fin, line, '\n');
    while (line != "##### END BOOK #####") {
        if (line == "### Title ###") {
            title = readBookTitle(fin);
        } else if (line == "### Publication Year ###") {
            publicationYear = readPublicationYear(fin);
        } else if (line == "### Price ###") {
            price = readPrice(fin);
        } else if (line == "### RATINGS ###") {
            ratings = readRatings(fin);
        } else if (line == "### CONTENT ROWS ###") {
            content = readContent(fin);
        } else if (line == "### Author Name ###") {
            authorName = readPersonName(fin);
        } else {
            std::cerr << "Section name: "
                      << "|" << line << "|" << std::endl;
            assert(false && "Invalid file format - invalid section name");
        }
        getline(fin, line, '\n');
    }

    return std::make_shared<model::book::Book>(*title, content, *authorName, *price, ratings, *publicationYear);
}

std::shared_ptr<std::string>
librarysystem::controller::deserializing::LibraryLoader::readBookTitle(std::ifstream &fin) {
    std::string line;
    getline(fin, line, '\n');
    return std::make_shared<std::string>(line);
}

std::shared_ptr<int> librarysystem::controller::deserializing::LibraryLoader::readPublicationYear(std::ifstream &fin) {
    std::string line;
    getline(fin, line, '\n');
    return std::make_shared<int>(utilities::StringToIntegerConverter::convert(line));
}

std::shared_ptr<double>
librarysystem::controller::deserializing::LibraryLoader::readPrice(std::ifstream &fin) {
    std::string line;
    getline(fin, line, '\n');
    return std::make_shared<double>(utilities::StringToDoubleConverter::convert(line));
}

std::shared_ptr<librarysystem::utilities::Container<double>>
librarysystem::controller::deserializing::LibraryLoader::readRatings(std::ifstream &fin) {
    std::string line;
    std::shared_ptr<utilities::Container<double>> ratings = std::make_shared<utilities::Container<double>>();
    getline(fin, line, '\n');
    while (line != "### END RATINGS ###") {
        ratings->insert(utilities::StringToDoubleConverter::convert(line));
        getline(fin, line, '\n');
    }
    return ratings;
}

std::shared_ptr<librarysystem::model::book::Content>
librarysystem::controller::deserializing::LibraryLoader::readContent(std::ifstream &fin) {
    std::string line;
    std::shared_ptr<utilities::Container<std::string>> rows = std::make_shared<utilities::Container<std::string>>();
    getline(fin, line, '\n');
    while (line != "### END CONTENT ROWS ###") {
        rows->insert(line);
        getline(fin, line, '\n');
    }

    return std::make_shared<model::book::Content>(rows);
}

std::shared_ptr<std::string>
librarysystem::controller::deserializing::LibraryLoader::readPersonName(std::ifstream &fin) {
    std::string line;
    getline(fin, line, '\n');
    return std::make_shared<std::string>(line);
}

std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::person::Author>>>
librarysystem::controller::deserializing::LibraryLoader::readAuthors(std::ifstream &fin,
                                                                     const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books) {
    std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>> authors =
            std::make_shared<utilities::Container<std::shared_ptr<model::person::Author>>>();
    std::string line;
    getline(fin, line, '\n');
    while (line != "####### END AUTHORS #######") {
        if (line == "##### AUTHOR #####") {
            std::shared_ptr<Author> singleAuthor = readSingleAuthor(fin, books);
            authors->insert(singleAuthor);
        } else {
            std::cerr << "Section name: "
                      << "|" << line << "|" << std::endl;
            assert(false && "Invalid file format - invalid section name");
        }
        getline(fin, line, '\n');
    }
    return authors;
}


std::shared_ptr<librarysystem::model::person::Author>
librarysystem::controller::deserializing::LibraryLoader::readSingleAuthor(std::ifstream &fin,
                                                                          const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books) {
    std::shared_ptr<std::string> authorName;
    std::shared_ptr<std::string> nationality;
    std::shared_ptr<int> age;
    std::shared_ptr<utilities::Container<std::shared_ptr<Book>>> writtenBooks;
    std::shared_ptr<utilities::Container<std::shared_ptr<Book>>> readBooks;
    std::string line;
    getline(fin, line, '\n');
    while (line != "##### END AUTHOR #####") {
        if (line == "### Name ###") {
            authorName = readPersonName(fin);
        } else if (line == "### Age ###") {
            age = readAge(fin);
        } else if (line == "### Nationality ###") {
            nationality = readNationality(fin);
        } else if (line == "### WRITTEN BOOKS ###") {
            writtenBooks = readWrittenBooks(fin, books, *authorName);
        } else if (line == "### READ BOOKS ###") {
            readBooks = readReadBooks(fin, books);
        } else {
            std::cerr << "Section name: "
                      << "|" << line << "|" << std::endl;
            assert(false && "Invalid file format - invalid section name");
        }
        getline(fin, line, '\n');
    }
    return std::make_shared<Author>(*authorName, *age, *nationality, writtenBooks, readBooks);
}

std::shared_ptr<int> librarysystem::controller::deserializing::LibraryLoader::readAge(std::ifstream &fin) {
    std::string line;
    getline(fin, line, '\n');
    int number = utilities::StringToIntegerConverter::convert(line);
    return std::make_shared<int>(utilities::StringToIntegerConverter::convert(line));
}

std::shared_ptr<std::string>
librarysystem::controller::deserializing::LibraryLoader::readNationality(std::ifstream &fin) {
    std::string line;
    getline(fin, line, '\n');
    return std::make_shared<std::string>(line);
}

std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>>
librarysystem::controller::deserializing::LibraryLoader::readWrittenBooks(std::ifstream &fin,
                                                                          const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books,
                                                                          const std::string &authorName) {
    std::shared_ptr<utilities::Container<std::shared_ptr<Book>>> writtenBooks = std::make_shared<utilities::Container<std::shared_ptr<Book>>>();
    std::string bookTitle;
    getline(fin, bookTitle, '\n');
    while (bookTitle != "### END WRITTEN BOOKS ###") {
        std::shared_ptr<Book> singleBook = extractSingleBook(bookTitle, authorName, books);
        writtenBooks->insert(singleBook);
        getline(fin, bookTitle, '\n');
    }
    return writtenBooks;
}

std::shared_ptr<librarysystem::model::book::Book>
librarysystem::controller::deserializing::LibraryLoader::extractSingleBook(const std::string &bookTitle,
                                                                           const std::string &authorName,
                                                                           const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books) {
    std::shared_ptr<Book> fictiveBook = std::make_shared<Book>(bookTitle, authorName);
    int idx = books->search(fictiveBook,
                            [](const std::shared_ptr<Book> &first, const std::shared_ptr<Book> &second) {
                                return *first == *second;
                            });
    return books->getElement(idx);
}


std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>>
librarysystem::controller::deserializing::LibraryLoader::readReadBooks(std::ifstream &fin,
                                                                       const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books) {
    std::shared_ptr<utilities::Container<std::shared_ptr<Book>>> readBooks = std::make_shared<utilities::Container<std::shared_ptr<Book>>>();
    std::string bookTitle;
    std::string authorName;
    getline(fin, bookTitle, '\n');
    while (bookTitle != "### END READ BOOKS ###") {
        getline(fin, authorName, '\n');
        std::shared_ptr<Book> singleBook = extractSingleBook(bookTitle, authorName, books);
        readBooks->insert(singleBook);
        getline(fin, bookTitle, '\n');
    }
    return readBooks;
}

std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::person::Reader>>>
librarysystem::controller::deserializing::LibraryLoader::readReaders(std::ifstream &fin,
                                                                     const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books) {
    std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>> readers =
            std::make_shared<utilities::Container<std::shared_ptr<model::person::Reader>>>();
    std::string line;
    getline(fin, line, '\n');
    while (line != "####### END READERS #######") {
        if (line == "##### READER #####") {
            std::shared_ptr<Reader> singleReader = readSingleReader(fin, books);
            readers->insert(singleReader);
        } else {
            std::cerr << "Section name: "
                      << "|" << line << "|" << std::endl;
            assert(false && "Invalid file format - invalid section name");
        }
        getline(fin, line, '\n');
    }
    return readers;
}

std::shared_ptr<librarysystem::model::person::Reader>
librarysystem::controller::deserializing::LibraryLoader::readSingleReader(std::ifstream &fin,
                                                                          const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books) {
    std::shared_ptr<std::string> readerName;
    std::shared_ptr<std::string> nationality;
    std::shared_ptr<int> age;
    std::shared_ptr<utilities::Container<std::shared_ptr<Book>>> readBooks;
    std::string line;
    getline(fin, line, '\n');
    while (line != "##### END READER #####") {
        if (line == "### Name ###") {
            readerName = readPersonName(fin);
        } else if (line == "### Age ###") {
            age = readAge(fin);
        } else if (line == "### Nationality ###") {
            nationality = readNationality(fin);
        } else if (line == "### READ BOOKS ###") {
            readBooks = readReadBooks(fin, books);
        } else {
            std::cerr << "Section name: "
                      << "|" << line << "|" << std::endl;
            assert(false && "Invalid file format - invalid section name");
        }
        getline(fin, line, '\n');
    }
    return std::make_shared<Reader>(*readerName, *age, *nationality, readBooks);
}

void librarysystem::controller::deserializing::LibraryLoader::checkIntegerNumberLine(const std::string &line) {
    assert(containsOnlyDigits(line) && canFitInInt(line));
}

bool librarysystem::controller::deserializing::LibraryLoader::containsOnlyDigits(const std::string &line) {
    for (int i = 0; i < line.length(); i++) {
        if (!('0' <= line[i] && line[i] <= '9')) {
            return false;
        }
    }
    return true;
}

bool librarysystem::controller::deserializing::LibraryLoader::canFitInInt(const std::string &line) {
    return containsOnlyDigits(line) && line.length() < 1e9;
}
