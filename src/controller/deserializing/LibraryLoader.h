#ifndef LIBRARYLOADER_H_
#define LIBRARYLOADER_H_

#include <memory>
#include <controller/library/Library.h>

namespace librarysystem {

    namespace controller {

        namespace deserializing {

            class LibraryLoader {
            public:
                LibraryLoader();

                std::shared_ptr<controller::library::Library> load(const std::string &absoluteFilename);

            private:
                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> readBooks(std::ifstream &fin);

                std::shared_ptr<model::book::Book> readSingleBook(std::ifstream &fin);

                std::shared_ptr<std::string> readBookTitle(std::ifstream &fin);

                std::shared_ptr<int> readPublicationYear(std::ifstream &fin);

                std::shared_ptr<double> readPrice(std::ifstream &fin);

                std::shared_ptr<utilities::Container<double>> readRatings(std::ifstream &fin);

                std::shared_ptr<model::book::Content> readContent(std::ifstream &fin);

                std::shared_ptr<std::string> readPersonName(std::ifstream &fin);

                std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>>
                readAuthors(std::ifstream &fin,
                            const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books);

                std::shared_ptr<model::person::Author> readSingleAuthor(std::ifstream &fin,
                                                                        const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books);

                std::shared_ptr<int> readAge(std::ifstream &fin);

                std::shared_ptr<std::string> readNationality(std::ifstream &fin);

                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>>
                readWrittenBooks(std::ifstream &fin,
                                 const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books,
                                 const std::string &authorName);

                std::shared_ptr<model::book::Book>
                extractSingleBook(const std::string &bookTitle, const std::string &authorName,
                                  const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books);

                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>>
                readReadBooks(std::ifstream &fin,
                              const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books);

                std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>>
                readReaders(std::ifstream &fin,
                            const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books);

                std::shared_ptr<model::person::Reader> readSingleReader(std::ifstream &fin,
                                                                        const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &books);

                void checkIntegerNumberLine(const std::string &line);

                bool containsOnlyDigits(const std::string &line);

                bool canFitInInt(const std::string &line);
            };

        };

    };

};

#endif
