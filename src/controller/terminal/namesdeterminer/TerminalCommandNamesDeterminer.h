//
// Created by mctakeout on 12.04.20 г..
//

#ifndef LIBRARYSYSTEM_TERMINALCOMMANDNAMESDETERMINER_H
#define LIBRARYSYSTEM_TERMINALCOMMANDNAMESDETERMINER_H

#include <iostream>
#include <memory>
#include "utilities/Container.hpp"
#include "controller/terminal/commands/OpenCommand.h"
#include "controller/terminal/commands/CloseCommand.h"
#include "controller/terminal/commands/ExitCommand.h"

namespace librarysystem {

    namespace controller {

        namespace terminal {

            namespace namesdeterminer {

                class TerminalCommandNamesDeterminer {
                private:
                    static std::shared_ptr<utilities::Container<std::string>> names;

                public:
                    /**
                     * Add new TerminalCommands here.
                     * @return terminal command names
                     */
                    static std::shared_ptr<utilities::Container<std::string>> getCommandNames() {
                        std::shared_ptr<utilities::Container<std::string>> names = std::make_shared<utilities::Container<std::string>>();
                        names->insert(commands::OpenCommand::NAME);
                        names->insert(commands::CloseCommand::NAME);
                        names->insert(commands::ExitCommand::NAME);
                        return names;
                    }

                    static bool isCommandTerminal(const std::string &command);
                };

            };

        };

    };

};


#endif //LIBRARYSYSTEM_TERMINALCOMMANDNAMESDETERMINER_H
