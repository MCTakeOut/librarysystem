//
// Created by mctakeout on 12.04.20 г..
//

#include "TerminalCommandNamesDeterminer.h"

std::shared_ptr<librarysystem::utilities::Container<std::string>> librarysystem::controller::terminal::namesdeterminer::TerminalCommandNamesDeterminer::names = TerminalCommandNamesDeterminer::getCommandNames();

bool librarysystem::controller::terminal::namesdeterminer::TerminalCommandNamesDeterminer::isCommandTerminal(const std::string &command) {
    return names->contains(command);
}
