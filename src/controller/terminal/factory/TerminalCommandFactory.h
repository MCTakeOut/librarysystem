#ifndef TERMINALCOMMANDFACTORY_H_
#define TERMINALCOMMANDFACTORY_H_

#include <unordered_map>
#include <controller/terminal/commands/OpenCommand.h>
#include <controller/terminal/commands/CloseCommand.h>
#include <controller/terminal/commands/ExitCommand.h>


namespace librarysystem {

    namespace controller {

        namespace terminal {

            namespace factory {

                class TerminalCommandFactory {
                private:
                    static std::unordered_map<std::string, std::shared_ptr<controller::terminal::commands::TerminalCommand>> commands;
                public:
                    TerminalCommandFactory();

                    ~TerminalCommandFactory();

                    static std::unordered_map<std::string, std::shared_ptr<controller::terminal::commands::TerminalCommand>>
                    createCommands() {
                        std::unordered_map<std::string, std::shared_ptr<controller::terminal::commands::TerminalCommand>> commandsHolder;
                        commandsHolder.insert(
                                std::make_pair(controller::terminal::commands::OpenCommand::NAME,
                                               std::make_shared<controller::terminal::commands::OpenCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::terminal::commands::CloseCommand::NAME,
                                               std::make_shared<controller::terminal::commands::CloseCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::terminal::commands::ExitCommand::NAME,
                                               std::make_shared<controller::terminal::commands::ExitCommand>()));
                        return commandsHolder;
                    }

                    static std::shared_ptr<controller::terminal::commands::TerminalCommand>
                    getCommand(const std::string &commandName);
                };

            };

        };

    };

};

#endif