#include "TerminalCommandFactory.h"

using librarysystem::controller::terminal::commands::TerminalCommand;

std::unordered_map<std::string, std::shared_ptr<TerminalCommand>> librarysystem::controller::terminal::factory::TerminalCommandFactory::commands = librarysystem::controller::terminal::factory::TerminalCommandFactory::createCommands();

librarysystem::controller::terminal::factory::TerminalCommandFactory::TerminalCommandFactory() {
};

librarysystem::controller::terminal::factory::TerminalCommandFactory::~TerminalCommandFactory() {

};

std::shared_ptr<TerminalCommand> librarysystem::controller::terminal::factory::TerminalCommandFactory::getCommand(const std::string &commandName) {
    std::unordered_map<std::string, std::shared_ptr<TerminalCommand>>::const_iterator it = commands.find(commandName);
    return it == commands.end() ? nullptr : it->second;
}
