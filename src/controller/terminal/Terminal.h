#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <memory>

#include "utilities/Container.hpp"

namespace librarysystem {
    namespace controller {
        namespace library {
            class Library;
        }
    }
}


namespace librarysystem {

    namespace controller {

        namespace terminal {

            class Terminal {
            private:
                std::shared_ptr<controller::library::Library> library;
            public:
                Terminal();

                void run();

            private:
                std::string extractCommandName(const std::string &commandLine);

                std::pair<std::string, std::string> getCommandNameAndArguments(
                        const std::string &commandLine);

                std::shared_ptr<utilities::Container<std::string>> extractArguments(
                const std::string &commandLine);

                std::string parseStringArgument(const std::string &argumentString, int i);

                std::string parseNumberArgument(const std::string &argumentsString, int i);
            };

        };

    };

};

#endif
