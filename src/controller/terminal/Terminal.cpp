#include "Terminal.h"
#include "controller/library/Library.h"
#include <iostream>
#include "controller/terminal/commands/OpenCommand.h"
#include "controller/terminal/commands/TerminalCommand.h"
#include "controller/terminal/factory/TerminalCommandFactory.h"
#include "controller/library/factory/LibraryCommandFactory.h"
#include "controller/library/namesdeterminer/LibraryCommandNamesDeterminer.h"
#include "controller/terminal/namesdeterminer/TerminalCommandNamesDeterminer.h"

using namespace librarysystem::controller::terminal::factory;
using namespace librarysystem::controller::terminal::namesdeterminer;
using namespace librarysystem::controller::terminal::commands;
using namespace librarysystem::controller::library::factory;
using namespace librarysystem::controller::library::namesdeterminer;
using namespace librarysystem::controller::library::commands;

librarysystem::controller::terminal::Terminal::Terminal() : library(nullptr) {
}

void librarysystem::controller::terminal::Terminal::run() {
    bool terminalAlive = true;
    bool waitForOpenCommand = true;
    while (terminalAlive) {
        std::string commandLine;
        std::getline(std::cin, commandLine, '\n');
        std::pair<std::string, std::string> commandPair = getCommandNameAndArguments(commandLine);
        std::shared_ptr<librarysystem::utilities::Container<std::string>> arguments = extractArguments(
                commandPair.second);
        if (TerminalCommandNamesDeterminer::isCommandTerminal(commandPair.first)) {
            std::shared_ptr<TerminalCommand> command = TerminalCommandFactory::getCommand(commandPair.first);
            this->library = command->execute(*arguments, terminalAlive);
            waitForOpenCommand = this->library == nullptr;
        } else if (LibraryCommandNamesDeterminer::isCommandLibrary(commandPair.first) && !waitForOpenCommand) {
            std::shared_ptr<LibraryCommand> command = LibraryCommandFactory::getCommand(commandPair.first);
            command->execute(*arguments, this->library);
        } else {
            std::cerr << "Invalid command!" << std::endl;
        }

    }
}

std::pair<std::string, std::string> librarysystem::controller::terminal::Terminal::getCommandNameAndArguments(
        const std::string &commandLine) {
    char nameDelimiter = '\"';
    size_t idx = commandLine.find(nameDelimiter);
    if (idx != std::string::npos) {
        return std::make_pair(commandLine.substr(0, idx - 1),
                              commandLine.substr(idx));
    } else {
        return std::make_pair(commandLine.substr(0), "");
    }
}

std::shared_ptr<librarysystem::utilities::Container<std::string>>
librarysystem::controller::terminal::Terminal::extractArguments(
        const std::string &argumentsString) {
    std::shared_ptr<librarysystem::utilities::Container<std::string>> arguments = std::make_shared<
            librarysystem::utilities::Container<std::string>>();

    int i = 0;
    while (i < argumentsString.length()) {
        if (argumentsString[i] == '\"') {
            std::string stringArgument = parseStringArgument(argumentsString,
                                                             i);
            if (stringArgument == "") {
                return nullptr;
            }
            i += stringArgument.length() + 1;
            arguments->insert(stringArgument);
        } else if ('0' <= argumentsString[i] && argumentsString[i] <= '9') {
            std::string numberArgument = parseNumberArgument(argumentsString,
                                                             i);
            i += numberArgument.length() - 1;
            arguments->insert(numberArgument);
        } else if (argumentsString[i] != ' ') {
            std::cerr << "Invalid command arguments format" << std::endl;
            return nullptr;
        }

        i++;
    }

    return arguments;
}

std::string librarysystem::controller::terminal::Terminal::parseStringArgument(const std::string &argumentString,
                                                                               int i) {
    i++; // skip first '"'
    int j = i;
    while (j < argumentString.length()) {
        if (argumentString[j] == '\"') {
            return argumentString.substr(i, j - i);
        }
        j++;
    }

    std::cerr << "Invalid string argument format - missing \"" << std::endl;
    return "";
}

std::string librarysystem::controller::terminal::Terminal::parseNumberArgument(const std::string &argumentsString,
                                                                               int i) {
    int j = i;
    while (j < argumentsString.length()) {
        if (!('0' <= argumentsString[j] && argumentsString[j] <= '9')) {
            return argumentsString.substr(i, j - i);
        }
        j++;
    }
    return argumentsString.substr(i, j - i);
}
