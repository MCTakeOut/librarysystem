#ifndef CLOSECOMMAND_H_
#define CLOSECOMMAND_H_

#include <memory>
#include <string>

#include "TerminalCommand.h"
#include "utilities/Container.hpp"

namespace librarysystem {

    namespace controller {

        namespace terminal {

            namespace commands {

                class CloseCommand : public TerminalCommand {
                public:
                    CloseCommand();

                    virtual ~CloseCommand();

                    std::shared_ptr<controller::library::Library> execute(const utilities::Container<std::string> &arguments, bool &terminalAlive) override;

                    inline static const std::string NAME = "close";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 0;
                };

            };

        };

    };

};

#endif
