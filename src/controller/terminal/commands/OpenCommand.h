
#ifndef OPENCOMMAND_H_
#define OPENCOMMAND_H_

#include <memory>
#include <string>

#include "TerminalCommand.h"
#include "utilities/Container.hpp"


namespace librarysystem {

    namespace controller {

        namespace terminal {

            namespace commands {

                class OpenCommand : public TerminalCommand {
                public:
                    OpenCommand();

                    virtual ~OpenCommand();

                    std::shared_ptr<controller::library::Library> execute(const utilities::Container<std::string> &arguments, bool &terminalAlive) override;

                    inline static const std::string NAME = "open";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 1;
                };

            };

        };

    };

};


#endif
