#include "CloseCommand.h"

#include <iostream>
#include <memory>
#include <string>

#include "utilities/Container.hpp"
#include "controller/library/Library.h"

librarysystem::controller::terminal::commands::CloseCommand::CloseCommand() {}

librarysystem::controller::terminal::commands::CloseCommand::~CloseCommand() {}

std::shared_ptr<librarysystem::controller::library::Library> librarysystem::controller::terminal::commands::CloseCommand::execute(const librarysystem::utilities::Container<std::string> &arguments, bool &terminalAlive) {
    if (arguments.getSize() != CloseCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Close command" << std::endl;
        return nullptr;
    }

    return nullptr;
}
