#ifndef EXITCOMMAND_H_
#define EXITCOMMAND_H_

#include <memory>
#include <string>

#include "TerminalCommand.h"
#include "utilities/Container.hpp"


namespace librarysystem {

    namespace controller {

        namespace terminal {

            namespace commands {

                class ExitCommand : public TerminalCommand {
                public:
                    ExitCommand();

                    virtual ~ExitCommand();

                    std::shared_ptr<controller::library::Library> execute(const utilities::Container<std::string> &arguments, bool &terminalAlive) override;

                    inline static const std::string NAME = "exit";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 0;
                };

            };

        };

    };

};

#endif
