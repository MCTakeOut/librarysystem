#ifndef TERMINALCOMMAND_H_
#define TERMINALCOMMAND_H_

#include <iostream>

#include "utilities/Container.hpp"
#include "controller/library/Library.h"

namespace librarysystem {

    namespace controller {

        namespace terminal {

            namespace commands {

                class TerminalCommand {
                public:
                    TerminalCommand();

                    ~TerminalCommand();

                    virtual std::shared_ptr<controller::library::Library> execute(const utilities::Container<std::string> &arguments, bool &terminalAlive) = 0;
                };

            };

        };

    };

};

#endif
