#include "ExitCommand.h"

#include <cassert>
#include <iostream>
#include <memory>
#include <string>

#include "utilities/Container.hpp"
#include "controller/library/Library.h"

librarysystem::controller::terminal::commands::ExitCommand::ExitCommand() {
}

librarysystem::controller::terminal::commands::ExitCommand::~ExitCommand() {
}

std::shared_ptr<librarysystem::controller::library::Library> librarysystem::controller::terminal::commands::ExitCommand::execute(const librarysystem::utilities::Container<std::string> &arguments, bool &terminalAlive) {
    if (arguments.getSize() != ExitCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Exit command" << std::endl;
        return nullptr;
    }
    terminalAlive = false;
    return nullptr;
}
