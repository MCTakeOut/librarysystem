
#include "OpenCommand.h"

#include <cassert>
#include <iostream>
#include <memory>
#include <string>

#include "utilities/Container.hpp"
#include "controller/deserializing/LibraryLoader.h"
#include "controller/library/Library.h"

librarysystem::controller::terminal::commands::OpenCommand::OpenCommand() {
}

librarysystem::controller::terminal::commands::OpenCommand::~OpenCommand() {
}

std::shared_ptr<librarysystem::controller::library::Library> librarysystem::controller::terminal::commands::OpenCommand::execute(const librarysystem::utilities::Container<std::string> &arguments, bool &terminalAlive) {
    if (arguments.getSize() != OpenCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Open command" << std::endl;
        return nullptr;
    }

    deserializing::LibraryLoader libraryLoader;
    return libraryLoader.load(arguments.getElement(0));
}
