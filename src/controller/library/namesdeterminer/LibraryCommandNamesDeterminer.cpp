//
// Created by mctakeout on 12.04.20 г..
//

#include "LibraryCommandNamesDeterminer.h"

std::shared_ptr<librarysystem::utilities::Container<std::string>> librarysystem::controller::library::namesdeterminer::LibraryCommandNamesDeterminer::names = LibraryCommandNamesDeterminer::getCommandNames();


bool librarysystem::controller::library::namesdeterminer::LibraryCommandNamesDeterminer::isCommandLibrary(const std::string &command) {
    return names->contains(command);
}