//
// Created by mctakeout on 12.04.20 г..
//

#ifndef LIBRARYSYSTEM_LIBRARYCOMMANDNAMESDETERMINER_H
#define LIBRARYSYSTEM_LIBRARYCOMMANDNAMESDETERMINER_H

#include <iostream>
#include <memory>
#include <controller/library/commands/DisplayAllContentFromBookCommand.h>
#include "controller/library/commands/SaveCommand.h"
#include "controller/library/commands/SaveAsCommand.h"
#include "controller/library/commands/AddBookCommand.h"
#include "controller/library/commands/AddAuthorCommand.h"
#include <controller/library/commands/AddReaderCommand.h>

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace namesdeterminer {

                class LibraryCommandNamesDeterminer {
                private:
                    static std::shared_ptr<utilities::Container<std::string>> names;
                public:
                    static std::shared_ptr<utilities::Container<std::string>> getCommandNames() {
                        std::shared_ptr<utilities::Container<std::string>>
                                names = std::make_shared<utilities::Container<std::string>>();
                        names->insert(controller::library::commands::SaveCommand::NAME);
                        names->insert(controller::library::commands::SaveAsCommand::NAME);
                        names->insert(controller::library::commands::AddBookCommand::NAME);
                        names->insert(controller::library::commands::AddAuthorCommand::NAME);
                        names->insert(controller::library::commands::AddReaderCommand::NAME);
                        names->insert(controller::library::commands::DisplayAllContentFromBookCommand::NAME);
                        return names;
                    }

                    static bool isCommandLibrary(const std::string &command);
                };

            };

        };

    };

};

#endif //LIBRARYSYSTEM_LIBRARYCOMMANDNAMESDETERMINER_H
