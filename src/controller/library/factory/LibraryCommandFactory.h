#ifndef LIBRARYCOMMANDFACTORY_H_
#define LIBRARYCOMMANDFACTORY_H_


#include <unordered_map>
#include <controller/library/commands/SaveCommand.h>
#include <controller/library/commands/SaveAsCommand.h>
#include <controller/library/commands/AddBookCommand.h>
#include <controller/library/commands/AddAuthorCommand.h>
#include <controller/library/commands/AddReaderCommand.h>
#include <controller/library/commands/DisplayAllContentFromBookCommand.h>

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace factory {

                class LibraryCommandFactory {
                private:
                    static std::unordered_map<std::string, std::shared_ptr<controller::library::commands::LibraryCommand>> commands;
                public:
                    LibraryCommandFactory();

                    ~LibraryCommandFactory();

                    static std::shared_ptr<controller::library::commands::LibraryCommand>

                    getCommand(const std::string &commandName);

                    static std::unordered_map<std::string, std::shared_ptr<controller::library::commands::LibraryCommand>>
                    createCommands() {
                        std::unordered_map<std::string, std::shared_ptr<controller::library::commands::LibraryCommand>> commandsHolder;
                        commandsHolder.insert(
                                std::make_pair(controller::library::commands::SaveCommand::NAME,
                                               std::make_shared<controller::library::commands::SaveCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::library::commands::SaveAsCommand::NAME,
                                               std::make_shared<controller::library::commands::SaveAsCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::library::commands::AddBookCommand::NAME,
                                               std::make_shared<controller::library::commands::AddBookCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::library::commands::AddAuthorCommand::NAME,
                                               std::make_shared<controller::library::commands::AddAuthorCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::library::commands::AddReaderCommand::NAME,
                                               std::make_shared<controller::library::commands::AddReaderCommand>()));
                        commandsHolder.insert(
                                std::make_pair(controller::library::commands::DisplayAllContentFromBookCommand::NAME,
                                               std::make_shared<controller::library::commands::DisplayAllContentFromBookCommand>()));
                        return commandsHolder;
                    }
                };

            };

        };

    };

};

#endif