#include "LibraryCommandFactory.h"

using namespace librarysystem::controller::library::commands;

std::unordered_map<std::string, std::shared_ptr<LibraryCommand>> librarysystem::controller::library::factory::LibraryCommandFactory::commands = librarysystem::controller::library::factory::LibraryCommandFactory::createCommands();

librarysystem::controller::library::factory::LibraryCommandFactory::LibraryCommandFactory() {
};

librarysystem::controller::library::factory::LibraryCommandFactory::~LibraryCommandFactory() {
};

std::shared_ptr<LibraryCommand> librarysystem::controller::library::factory::LibraryCommandFactory::getCommand(const std::string &commandName) {
    std::unordered_map<std::string, std::shared_ptr<LibraryCommand>>::const_iterator it = commands.find(commandName);
    return it == commands.end() ? nullptr : it->second;
}
