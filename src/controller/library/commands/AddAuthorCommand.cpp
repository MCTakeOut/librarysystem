//
// Created by mctakeout on 15.04.20 г..
//

#include "AddAuthorCommand.h"
#include "utilities/StringToIntegerConverter.h"

librarysystem::controller::library::commands::AddAuthorCommand::AddAuthorCommand() {}

librarysystem::controller::library::commands::AddAuthorCommand::~AddAuthorCommand() {

}

void librarysystem::controller::library::commands::AddAuthorCommand::execute(const utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) {
    if (arguments.getSize() != AddAuthorCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Add author command" << std::endl;
        return;
    }
    library->addAuthor(arguments.getElement(0), librarysystem::utilities::StringToIntegerConverter::convert(arguments.getElement(1)), arguments.getElement(2));
}
