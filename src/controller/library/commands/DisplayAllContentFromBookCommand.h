//
// Created by mctakeout on 29.04.20 г..
//

#ifndef LIBRARYSYSTEM_DISPLAYALLCONTENTFROMBOOKCOMMAND_H
#define LIBRARYSYSTEM_DISPLAYALLCONTENTFROMBOOKCOMMAND_H

#include "LibraryCommand.h"

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class DisplayAllContentFromBookCommand : public LibraryCommand {
                public:
                    DisplayAllContentFromBookCommand();

                    virtual ~DisplayAllContentFromBookCommand();

                    void execute(const utilities::Container<std::string> &arguments,
                                 const std::shared_ptr<Library> &library) override;

                    inline static const std::string NAME = "display all";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 2;
                };
            };
        };
    };
};


#endif //LIBRARYSYSTEM_DISPLAYALLCONTENTFROMBOOKCOMMAND_H
