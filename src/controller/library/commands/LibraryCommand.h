#ifndef LIBRARYCOMMAND_H_
#define LIBRARYCOMMAND_H_

#include "controller/library/Library.h"
#include <iostream>

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class LibraryCommand {
                public:
                    LibraryCommand();

                    ~LibraryCommand();

                    virtual void execute(const utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) = 0;
                };

            };

        };

    };

};

#endif
