#ifndef ADDAUTHORCOMMAND_H_
#define ADDAUTHORCOMMAND_H_

#include "LibraryCommand.h"

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class AddAuthorCommand : public LibraryCommand {
                public:
                    AddAuthorCommand();

                    virtual ~AddAuthorCommand();

                    void execute(const utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) override;

                    inline static const std::string NAME = "add author";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 3;
                };

            };

        };

    };

};

#endif
