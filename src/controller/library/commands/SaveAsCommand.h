#ifndef SAVEASCOMMAND_H_
#define SAVEASCOMMAND_H_

#include <memory>
#include <string>

#include "LibraryCommand.h"
#include "utilities/Container.hpp"

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class SaveAsCommand : public LibraryCommand {
                public:
                    SaveAsCommand();

                    virtual ~SaveAsCommand();

                    void execute(const utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) override;

                    inline static const std::string NAME = "save as";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 1;
                };

            };

        };

    };

};

#endif
