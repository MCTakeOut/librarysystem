//
// Created by mctakeout on 29.04.20 г..
//

#ifndef LIBRARYSYSTEM_ADDREADERCOMMAND_H
#define LIBRARYSYSTEM_ADDREADERCOMMAND_H

#include "LibraryCommand.h"

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class AddReaderCommand : public LibraryCommand {
                public:
                    AddReaderCommand();

                    virtual ~AddReaderCommand();

                    void execute(const utilities::Container<std::string> &arguments,
                                 const std::shared_ptr<Library> &library) override;

                    inline static const std::string NAME = "add reader";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 3;
                };
            };
        };
    };
};

#endif //LIBRARYSYSTEM_ADDREADERCOMMAND_H
