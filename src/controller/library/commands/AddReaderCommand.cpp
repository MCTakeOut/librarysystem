//
// Created by mctakeout on 29.04.20 г..
//

#include <utilities/StringToIntegerConverter.h>
#include "AddReaderCommand.h"

librarysystem::controller::library::commands::AddReaderCommand::AddReaderCommand() {};

librarysystem::controller::library::commands::AddReaderCommand::~AddReaderCommand() {};

void librarysystem::controller::library::commands::AddReaderCommand::execute(
        const librarysystem::utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) {
    if (arguments.getSize() != AddReaderCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Add reader command" << std::endl;
        return;
    }
    library->addReader(arguments.getElement(0), utilities::StringToIntegerConverter::convert(arguments.getElement(1)), arguments.getElement(2));

}
