//
// Created by mctakeout on 29.04.20 г..
//

#include "DisplayAllContentFromBookCommand.h"

librarysystem::controller::library::commands::DisplayAllContentFromBookCommand::DisplayAllContentFromBookCommand() {}

librarysystem::controller::library::commands::DisplayAllContentFromBookCommand::~DisplayAllContentFromBookCommand() {

}

void librarysystem::controller::library::commands::DisplayAllContentFromBookCommand::execute(
        const librarysystem::utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) {
    if (arguments.getSize() != DisplayAllContentFromBookCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Display all command" << std::endl;
        return;
    }
    library->displayAllContent(arguments.getElement(0), arguments.getElement(1));
}
