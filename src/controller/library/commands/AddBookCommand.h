#ifndef ADDBOOKCOMMAND_H_
#define ADDBOOKCOMMAND_H_


#include "LibraryCommand.h"

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class AddBookCommand : public LibraryCommand {
                public:
                    AddBookCommand();

                    virtual ~AddBookCommand();

                    void execute(const utilities::Container <std::string> &arguments, const std::shared_ptr<Library> &library) override;

                    inline static const std::string NAME = "add book";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 4;
                };

            };

        };

    };

};

#endif
