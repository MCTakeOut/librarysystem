#include "SaveCommand.h"

#include <cassert>
#include <iostream>
#include <memory>
#include <string>

#include "utilities/Container.hpp"
#include "controller/deserializing/LibraryLoader.h"
#include "controller/library/Library.h"

librarysystem::controller::library::commands::SaveCommand::SaveCommand() {
}

librarysystem::controller::library::commands::SaveCommand::~SaveCommand() {
}

void librarysystem::controller::library::commands::SaveCommand::execute(const librarysystem::utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) {
    if (arguments.getSize() != SaveCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Save command" << std::endl;
        return;
    }
    library->save();
}
