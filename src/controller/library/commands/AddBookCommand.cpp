//
// Created by mctakeout on 15.04.20 г..
//

#include "AddBookCommand.h"
#include "utilities/StringToIntegerConverter.h"
#include "utilities/StringToDoubleConverter.h"

librarysystem::controller::library::commands::AddBookCommand::AddBookCommand() {}

librarysystem::controller::library::commands::AddBookCommand::~AddBookCommand() {}

void librarysystem::controller::library::commands::AddBookCommand::execute(const librarysystem::utilities::Container<std::string> &arguments, const std::shared_ptr<librarysystem::controller::library::Library> &library) {
    if (arguments.getSize() != AddBookCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Add book command" << std::endl;
        return;
    }
    library->addBook(arguments.getElement(0), librarysystem::utilities::StringToIntegerConverter::convert(arguments.getElement(1)),
                     arguments.getElement(2), librarysystem::utilities::StringToDoubleConverter::convert(arguments.getElement(3)));
}

