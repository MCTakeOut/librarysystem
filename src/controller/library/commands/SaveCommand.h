#ifndef SAVECOMMAND_H_
#define SAVECOMMAND_H_

#include <memory>
#include <string>

#include "LibraryCommand.h"
#include "utilities/Container.hpp"

namespace librarysystem {

    namespace controller {

        namespace library {

            namespace commands {

                class SaveCommand : public LibraryCommand {
                public:
                    SaveCommand();

                    virtual ~SaveCommand();

                    void execute(const utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) override;

                    inline static const std::string NAME = "save";
                    inline static const int NUMBER_OF_REQUIRED_ARGUMENTS = 0;
                };

            };

        };

    };

};

#endif
