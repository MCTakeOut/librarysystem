#include "SaveAsCommand.h"

#include "controller/library/Library.h"
#include <cassert>
#include <iostream>
#include <memory>
#include <string>

librarysystem::controller::library::commands::SaveAsCommand::SaveAsCommand() {};

librarysystem::controller::library::commands::SaveAsCommand::~SaveAsCommand() {};

void librarysystem::controller::library::commands::SaveAsCommand::execute(const utilities::Container<std::string> &arguments, const std::shared_ptr<Library> &library) {
    if (arguments.getSize() != SaveAsCommand::NUMBER_OF_REQUIRED_ARGUMENTS) {
        std::cerr << "Invalid arguments for Save as command" << std::endl;
        return;
    }
    library->saveAs(arguments.getElement(0));
}
