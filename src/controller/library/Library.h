#ifndef LIBRARY_H_
#define LIBRARY_H_

#include "model/person/Author.h"
#include "model/book/Book.h"
#include "utilities/Container.hpp"
#include "model/person/Reader.h"

namespace librarysystem {

    namespace controller {

        namespace library {

            class Library {

                std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> books;
                std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>> authors;
                std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>> readers;

            public:
                Library(std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> books,
                        std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>> authors,
                        std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>> readers);

                const std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> &getBooks() const;

                const std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>> &getAuthors() const;

                const std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>> &getReaders() const;

                void saveAs(const std::string &path);

                void save();

                void addBook(const std::shared_ptr<model::book::Book>& book);

                void
                addBook(const std::string &title, int publicationYear, const std::string &authorName, double price);

                void addBooks(const utilities::Container<std::shared_ptr<model::book::Book>> &books);

                void addAuthor(const std::shared_ptr<model::person::Author>& author);

                void addAuthor(const std::string &name, int age, const std::string &nationality);

                void addReader(const std::shared_ptr<model::person::Reader>& reader);

                void addReader(const std::string &name, int age, const std::string &nationality);

                std::shared_ptr<model::person::Author> searchPersonInAuthors(const std::string &name) const;

                std::shared_ptr<model::book::Book>
                searchBook(const std::string &title, const std::string &author) const;

                void displayAllContent(const std::string &title, const std::string &author) const;

                void
                displayContentFromRowTillEnd(const std::string &title, const std::string &author, int startRow) const;

                void dislpayContentInRange(const std::string &title, const std::string &author, int startRow,
                                           int endRow) const;
            };

        };

    };

};

#endif
