#include "Library.h"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>
#include <utility>

#include "model/person/Author.h"
#include "model/book/Book.h"
#include "utilities/Container.hpp"
#include "model/person/Reader.h"
#include "model/book/Content.h"
#include "controller/deserializing/LibraryLoader.h"

using namespace librarysystem::model::book;
using namespace librarysystem::model::person;
using namespace librarysystem::utilities;


librarysystem::controller::library::Library::Library(
        std::shared_ptr<utilities::Container<std::shared_ptr<model::book::Book>>> books,
        std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Author>>> authors,
        std::shared_ptr<utilities::Container<std::shared_ptr<model::person::Reader>>> readers)
        : books(std::move(books)), authors(std::move(authors)), readers(std::move(readers)) {}

const std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::book::Book>>> &
librarysystem::controller::library::Library::getBooks() const {
    return books;
}

const std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::person::Author>>> &
librarysystem::controller::library::Library::getAuthors() const {
    return authors;
}

const std::shared_ptr<librarysystem::utilities::Container<std::shared_ptr<librarysystem::model::person::Reader>>> &
librarysystem::controller::library::Library::getReaders() const {
    return readers;
}

void librarysystem::controller::library::Library::saveAs(const std::string &path) {
}

void librarysystem::controller::library::Library::save() {
}

void librarysystem::controller::library::Library::addBook(const std::shared_ptr<Book>& book) {

    books->insert(book);
}

void librarysystem::controller::library::Library::addBook(const std::string &title, int publicationYear,
                                                          const std::string &authorName, double price) {
    std::shared_ptr<Book> book = std::make_shared<Book>(title, publicationYear, authorName, price);
    books->insert(book);
}

void librarysystem::controller::library::Library::addBooks(const Container<std::shared_ptr<model::book::Book>> &books) {
    for (int i = 0; i < books.getSize(); ++i) {
        addBook(books.getElement(i));
    }
}

void librarysystem::controller::library::Library::addAuthor(const std::shared_ptr<Author>& author) {
    authors->insert(author);
}

void librarysystem::controller::library::Library::addAuthor(const std::string &name, int age,
                                                            const std::string &nationality) {
    std::shared_ptr<Author> author = std::make_shared<Author>(name, age, nationality);
    authors->insert(author);
}

void librarysystem::controller::library::Library::addReader(const std::shared_ptr<Reader>& reader) {
    readers->insert(reader);
}

void librarysystem::controller::library::Library::addReader(const std::string &name, int age,
                                                            const std::string &nationality) {
    std::shared_ptr<Reader> reader = std::make_shared<Reader>(name, age, nationality);
    readers->insert(reader);
}

std::shared_ptr<Book>
librarysystem::controller::library::Library::searchBook(const std::string &title, const std::string &author) const {
    std::shared_ptr<Book> fictiveBook = std::make_shared<Book>(title, author);
    int idx = books->search(fictiveBook,
                            [](const std::shared_ptr<Book> &first, const std::shared_ptr<Book> &second) {
                                return *first == *second;
                            });
    return books->getElement(idx);
}

std::shared_ptr<Author> librarysystem::controller::library::Library::searchPersonInAuthors(
        const std::string &name) const {
    for (int i = 0; i < authors->getSize(); i++) {
        std::shared_ptr<Author> author = authors->getElement(i);
        if (author->getName() == name) {
            return author;
        }
    }
    return nullptr;
}

void librarysystem::controller::library::Library::displayAllContent(const std::string &title,
                                                                    const std::string &author) const {
    displayContentFromRowTillEnd(title, author, 0);
}

void librarysystem::controller::library::Library::displayContentFromRowTillEnd(const std::string &title,
                                                                               const std::string &author,
                                                                               int startRow) const {
    dislpayContentInRange(title, author, startRow, Content::MAX_ROWS);
}

void
librarysystem::controller::library::Library::dislpayContentInRange(const std::string &title, const std::string &author,
                                                                   int startRow,
                                                                   int endRow) const {
    std::shared_ptr<Book> book = searchBook(title, author);
    assert(startRow <= endRow);
    endRow = std::min(endRow, book->getContent()->getNumberOfRows());
    for (int i = startRow; i < endRow; i++) {
        std::cout << i + 1 << " " << book->getContent()->getRow(i) << std::endl;
    }
}




