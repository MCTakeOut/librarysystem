#include "StringToDoubleConverter.h"

double librarysystem::utilities::StringToDoubleConverter::convert(const std::string &stringNumber) {
    return std::stod(stringNumber);
}
