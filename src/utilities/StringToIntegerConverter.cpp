#include "StringToIntegerConverter.h"

int librarysystem::utilities::StringToIntegerConverter::convert(const std::string stringNumber) {
    return std::stoi(stringNumber);
}
