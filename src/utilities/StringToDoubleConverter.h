#ifndef STRINGTODOUBLECONVERTER_H_
#define STRINGTODOUBLECONVERTER_H_

#include <string>

namespace librarysystem {

    namespace utilities {

        class StringToDoubleConverter {
        public:
            static double convert(const std::string &stringNumber);
        };

    };
};

#endif
