#ifndef STRINGTOINTEGERCONVERTER_H_
#define STRINGTOINTEGERCONVERTER_H_

#include <string>

namespace librarysystem {

    namespace utilities {

        class StringToIntegerConverter {
        public:
            static int convert(const std::string stringNumber);
        };
    };
};

#endif